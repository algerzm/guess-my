import { useRouter } from "next/router";
import styles from "../../styles/Book.module.css";
import AppBar from "../../components/template/AppBar";

import SideProfile from "../../components/book/SideProfile";
import QuestionItem from "../../components/book/QuestionItem";

const item1 = {
  imageUrl:
    "https://www.ngenespanol.com/wp-content/uploads/2018/08/Prueba-las-3-mejores-hamburguesas-de-Nueva-York-1280x720.jpg",
  text: "Hamburguesa",
};

const item2 = {
  imageUrl:
    "https://www.saborusa.com/wp-content/uploads/2019/12/origen-de-la-pizza-1.jpg",
  text: "Pizza",
};

const item3 = {
  imageUrl:
    "https://as.com/meristation/imagenes/2021/05/07/betech/1620421981_410581_1620422287_noticia_normal_recorte1.jpg",
  text: "Iron Man",
};

const item4 = {
  imageUrl:
    "https://www.lacasadeel.net/wp-content/uploads/2021/01/retorno-de-captain-america-al-mcu.jpg",
  text: "Capitan America",
};

export default function Book() {
  const router = useRouter();
  const { user } = router.query;

  return (
    <div className={styles.container}>
      <AppBar></AppBar>
      <SideProfile username={user} />
      <div className={styles.gridContainer}>
        <p className={styles.title}>What do you prefer?</p>
        <QuestionItem firstItem={item1} secondItem={item2} />
        <QuestionItem firstItem={item3} secondItem={item4} />
        <QuestionItem firstItem={item1} secondItem={item2} />
        <QuestionItem firstItem={item3} secondItem={item4} />
        <QuestionItem firstItem={item1} secondItem={item2} />
        <QuestionItem firstItem={item3} secondItem={item4} />
      </div>
    </div>
  );
}
