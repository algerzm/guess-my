import styles from "../styles/SignUp.module.css";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { signUpValidationSchema } from "../utils/validationSchema";

export default function signup() {
  const formOptions = {
    resolver: yupResolver(signUpValidationSchema),
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm(formOptions);

  const onSubmit = (data) => console.log(data);

  return (
    <form className={styles.container} onSubmit={handleSubmit(onSubmit)}>
      <div className={styles.formBox}>
        <p className={styles.title}>Sign Up</p>
        <div className={styles.inputsContainer}>
          <p className={styles.inputLabel}>Email:</p>
          <input
            {...register("email")}
            className={styles.inputWord}
            type="text"
          />
          {errors.email && (
            <span className={styles.errorLabel}>* {errors.email.message}</span>
          )}
          <p className={styles.inputLabel}>Username:</p>
          <input
            {...register("username")}
            className={styles.inputWord}
            type="text"
          />
          {errors.username && (
            <span className={styles.errorLabel}>
              * {errors.username.message}
            </span>
          )}
          <p className={styles.inputLabel}>Password:</p>
          <input
            name="password"
            {...register("password")}
            className={styles.inputWord}
            type="password"
          />
          {errors.password && (
            <span className={styles.errorLabel}>
              * {errors.password.message}
            </span>
          )}
          <p className={styles.inputLabel}>Password Confirm:</p>
          <input
            {...register("confirmPassword")}
            className={styles.inputWord}
            type="password"
          />
          {errors.confirmPassword && (
            <span className={styles.errorLabel}>
              * {errors.confirmPassword.message}
            </span>
          )}
        </div>
        <input
          type="submit"
          className={styles.submitButton}
          value="Sign Up Now!"
        />
      </div>
    </form>
  );
}
