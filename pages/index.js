import styles from "../styles/Home.module.css";
import AppBar from "../components/template/AppBar";

export default function Home() {
  return (
    <div className={styles.container}>
      <AppBar />
      <div className={styles.mainBox}>
        <h1 className={styles.textCenter}>Search for User:</h1>
        <div className={styles.inputContainer}>
          <input type="text" className={styles.inputWord} />
          <input type="button" value="Search" className={styles.submitButton} />
        </div>
      </div>
    </div>
  );
}
