import styles from "../styles/Login.module.css";
import { useForm } from "react-hook-form";
import { emailPattern, passwordPattern } from "../utils/validationPatterns";

export default function login() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => console.log(data);

  return (
    <form className={styles.container} onSubmit={handleSubmit(onSubmit)}>
      <div className={styles.formBox}>
        <p className={styles.title}>Sign In</p>
        <div className={styles.inputsContainer}>
          <p className={styles.inputLabel}>Email:</p>
          <input
            {...register("email", {
              required: "Email is required.",
              pattern: {
                value: emailPattern,
                message: "Email must be valid.",
              },
            })}
            className={styles.inputWord}
            type="text"
          />
          {errors.email && (
            <span className={styles.errorLabel}>* {errors.email.message}</span>
          )}
          <p className={styles.inputLabel}>Password:</p>
          <input
            {...register("password", {
              required: "Password is required.",
              pattern: {
                value: passwordPattern,
                message:
                  "Must be at least 8 characters and contain numbers and letters.",
              },
            })}
            className={styles.inputWord}
            type="password"
          />
          {errors.password && (
            <span className={styles.errorLabel}>
              * {errors.password.message}
            </span>
          )}
        </div>
        <input
          type="submit"
          className={styles.submitButton}
          value="Login Now!"
        />
      </div>
    </form>
  );
}
