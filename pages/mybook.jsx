import { useState } from "react";
import styles from "../styles/MyBook.module.css";
import AppBar from "../components/template/AppBar";
import Header from "../components/mybook/Header";
import BookItem from "../components/mybook/BookItem";
import NewItemModal from "../components/mybook/NewItemModal";
import ModifyItemModal from "../components/mybook/ModifyItemModal";

export default function mybook() {
  const [isNewOpen, setIsNewOpen] = useState(false);
  const [isModifyOpen, setIsModifyOpen] = useState(false);

  const openModal = () => setIsNewOpen(true);
  const closeModal = () => setIsNewOpen(false);

  const openModifyModal = () => setIsModifyOpen(true);
  const closeModifyModal = () => setIsModifyOpen(false);

  return (
    <div id="myPageContainer" className={styles.container}>
      <AppBar />
      <NewItemModal isOpen={isNewOpen} closeModal={closeModal} />
      <ModifyItemModal isOpen={isModifyOpen} closeModal={closeModifyModal} />
      <div id="innerContainer" className={styles.insideContainer}>
        <Header />
        <div className={styles.gridContainer}>
          <BookItem isAddItem openModal={openModal}></BookItem>
          <BookItem openModal={openModifyModal}></BookItem>
          <BookItem openModal={openModifyModal}></BookItem>
          <BookItem openModal={openModifyModal}></BookItem>
          <BookItem openModal={openModifyModal}></BookItem>
          <BookItem openModal={openModifyModal}></BookItem>
          <BookItem openModal={openModifyModal}></BookItem>
          <BookItem openModal={openModifyModal}></BookItem>
          <BookItem openModal={openModifyModal}></BookItem>
          <BookItem openModal={openModifyModal}></BookItem>
          <BookItem openModal={openModifyModal}></BookItem>
        </div>
      </div>
    </div>
  );
}
