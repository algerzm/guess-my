import { NextResponse, NextRequest, NextFetchEvent } from "next/server";

export default function _middleware(req, ev) {
  const { pathname } = req.nextUrl;
  if (pathname == "/loginss") {
    console.log("redirect");
    return NextResponse.redirect("/book/default");
  }

  return NextResponse.next();
}
