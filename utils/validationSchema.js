import { emailPattern, passwordPattern } from "./validationPatterns";
import * as Yup from "yup";

const signUpValidationSchema = Yup.object().shape({
  confirmPassword: Yup.string()
    .required("Confirm Password is required.")
    .oneOf([Yup.ref("password")], "Passwords must match."),
  password: Yup.string()
    .required("Password is required.")
    .matches(
      passwordPattern,
      "Must be at least 8 characters and contain numbers and letters."
    ),
  email: Yup.string()
    .required("Email is required.")
    .matches(emailPattern, "Email must be valid."),
  username: Yup.string()
    .required("Username is required")
    .min(6, "Must be at least 6 characters."),
});

export { signUpValidationSchema };
