import styles from "../../styles/QuestionItem.module.css";

export default function QuestionItem({ firstItem, secondItem }) {
  const defaultImage1 =
    "https://www.wallpapertip.com/wmimgs/29-295841_pink-geometric-wallpaper.jpg";
  const defaultImage2 =
    "https://besthqwallpapers.com/Uploads/31-8-2021/177117/thumb2-black-geometric-shapes-4k-black-low-poly-background-creative-geometric-textures.jpg";
  let image1 = firstItem.imageUrl ? firstItem.imageUrl : defaultImage1;
  let image2 = secondItem.imageUrl ? secondItem.imageUrl : defaultImage2;
  return (
    <div className={styles.questionContainer}>
      <div
        style={{
          backgroundImage: `url(${image1})`,
        }}
        className={styles.questionButton}
      >
        <p className={styles.innerQuestionText}>{firstItem.text}</p>
      </div>
      <p>Or</p>
      <div
        style={{
          backgroundImage: `url(${image2})`,
        }}
        className={styles.questionButton}
      >
        <p className={styles.innerQuestionText}>{secondItem.text}</p>
      </div>
    </div>
  );
}
