import styles from "../../styles/SideProfile.module.css";

export default function SideProfile({ username }) {
  return (
    <div className={styles.sideProfileContainer}>
      <img
        src="/avatar.png"
        alt="avatar"
        width="150"
        height="150"
        className={styles.imageShadows}
      />
      <h3 className={styles.textName}>{username}</h3>
      <div className={styles.infoContainer}>
        <h3 className={styles.textShadow}>Compatibility with others:</h3>
        <p className={styles.textShadow} style={{ marginTop: "-10px" }}>
          10%
        </p>
      </div>
      <div className={styles.infoContainer}>
        <h3 className={styles.textShadow}>Answered books:</h3>
        <p className={styles.textShadow} style={{ marginTop: "-10px" }}>
          230
        </p>
      </div>
      <button className={styles.finishButton}>Submit Answers</button>
      <div className={styles.anonymousCheckbox}>
        <input
          className={styles.checkboxStyle}
          type="checkbox"
          name="anonymously"
          id="cbAnonymously"
        />
        <label htmlFor="cbAnonymously" className={styles.checkboxText}>
          Send answers anonymously
        </label>
      </div>
    </div>
  );
}
