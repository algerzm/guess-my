import styles from "../../styles/AppBar.module.css";
import NavButton from "./NavButton";

export default function AppBar() {
  const NavButtonDivider = () => (
    <div className={styles.navButtonDivider}></div>
  );

  return (
    <div className={styles.appBarContainer}>
      <div className={styles.logoContainer}>
        <p>Guess My</p>
      </div>
      <div className={styles.buttonsContainer}>
        <NavButton title="My Book" />
        <NavButtonDivider />
        <NavButton title="Profile" />
        <NavButtonDivider />
        <NavButton title="Sign Out" />
      </div>
    </div>
  );
}
