import styles from "../../styles/AppBar.module.css";

export default function NavButton({ title }) {
  return (
    <div className={styles.navButtonContainer}>
      <p className={styles.navButtonText}>{title}</p>
      <div className={styles.navButtonDecorator}></div>
    </div>
  );
}
