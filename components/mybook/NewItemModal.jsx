import styles from "../../styles/NewItemModal.module.css";
import Modal from "react-modal";

export default function NewItemModal({ isOpen, closeModal }) {
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      width: "80%",
      maxWidth: "750px",
      padding: "1rem 2rem 1rem 2rem",
    },
    overlay: {
      backgroundColor: "rgba(0,0,0, 0.8)",
    },
  };

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={closeModal}
      style={customStyles}
      contentLabel="New Item Modal"
      closeTimeoutMS={200}
    >
      <div>
        <h2 className={styles.title}>Add New Book Item</h2>
        <p className={styles.subtitle}>Fill up both options to continue</p>
      </div>
      <div className={styles.divider}></div>
      <div className={styles.formContainer}>
        <div className={styles.inputContainer}>
          <p className={styles.inputLabel}>First Option Text</p>
          <input type="text" className={styles.inputWord} />
        </div>
        <div className={styles.inputContainer}>
          <p className={styles.inputLabel}>First Option Image</p>
          <input type="file" multiple="multiple" className={styles.inputFile} />
        </div>
      </div>
      <div className={styles.formContainer}>
        <div className={styles.inputContainer}>
          <p className={styles.inputLabel}>Second Option Text</p>
          <input type="text" className={styles.inputWord} />
        </div>
        <div className={styles.inputContainer}>
          <p className={styles.inputLabel}>Second Option Image</p>
          <input type="file" multiple="multiple" className={styles.inputFile} />
        </div>
      </div>
      <button className={styles.finishButton}>Submit</button>
    </Modal>
  );
}
