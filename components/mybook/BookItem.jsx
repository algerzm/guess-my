import styles from "../../styles/BookItem.module.css";
import ItemOption from "./ItemOption";
import AddItemOption from "./AddItemOption";

export default function BookItem({ isAddItem, openModal }) {
  const canOpenModal = () => {
    const { innerWidth: width, innerHeight: height } = window;
    if (width > 700) return null;

    openModal();
  };

  if (isAddItem) {
    return (
      <AddItemOption openModal={openModal} className={styles.itemContainer}>
        Add New Item
      </AddItemOption>
    );
  }

  return (
    <div className={styles.itemContainer} onClick={canOpenModal}>
      <div className={styles.optionsContainer}>
        <ItemOption></ItemOption>
        <div className={styles.divider}></div>
        <ItemOption></ItemOption>
      </div>
      <div className={styles.buttonsContainer}>
        <button className={styles.button} onClick={openModal}>
          <img src="/edit-white.png" height={30} width={30} alt="" />
        </button>
        <button className={styles.button}>
          <img src="/remove-white.png" height={35} width={35} alt="" />
        </button>
      </div>
    </div>
  );
}
