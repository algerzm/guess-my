import styles from "../../styles/ItemOption.module.css";

export default function ItemOption() {
  return (
    <div className={styles.itemOptionContainer}>
      <img
        className={styles.imageOption}
        src="/avatar.png"
        height="10%"
        alt=""
      />
      <p className={styles.optionText}>Este es una opcion</p>
    </div>
  );
}
