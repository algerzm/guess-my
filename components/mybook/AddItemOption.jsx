import styles from "../../styles/AddItemOption.module.css";

export default function AddItemOption({ openModal }) {
  return (
    <div className={styles.itemContainer} onClick={() => openModal()}>
      <img src="/add.png" alt="" height={30} width={30} />
      <p className={styles.text}>Add New Item</p>
    </div>
  );
}
