import styles from "../../styles/MyBookHeader.module.css";

export default function Header() {
  return (
    <div className={styles.container}>
      <div>
        <img
          src="/avatar.png"
          alt="avatar"
          height={220}
          width={220}
          className={styles.avatarImage}
        />
      </div>
      <div>
        <p className={styles.mainText}>Hey, Navi</p>
        <p className={styles.secondaryText}>
          Here you can Add, Modify and Remove your book items.
        </p>
      </div>
    </div>
  );
}
