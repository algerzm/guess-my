import styles from "../../styles/NewItemModal.module.css";
import Modal from "react-modal";
import InputFile from "./InputFile";

export default function ModifyItemModal({ isOpen, closeModal }) {
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      width: "80%",
      maxWidth: "600px",
      padding: "1rem 2rem 1rem 2rem",
    },
    overlay: {
      backgroundColor: "rgba(0,0,0, 0.8)",
    },
  };

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={closeModal}
      style={customStyles}
      contentLabel="Modify Modal"
      closeTimeoutMS={200}
    >
      <div>
        <h2 className={styles.title}>Modify Book Item</h2>
        <p className={styles.subtitle}>
          Choose info you want to change to te current item.
        </p>
      </div>
      <div className={styles.divider}></div>
      <div className={styles.formContainer}>
        <div className={styles.inputContainer}>
          <InputFile idName="first"></InputFile>
        </div>
        <div className={styles.inputContainer}>
          <p className={styles.inputLabel}>First Option Text</p>
          <input type="text" className={styles.inputWord} />
        </div>
      </div>
      <div className={styles.formContainer}>
        <div className={styles.inputContainer}>
          <InputFile idName="second"></InputFile>
        </div>
        <div className={styles.inputContainer}>
          <p className={styles.inputLabel}>Second Option Text</p>
          <input type="text" className={styles.inputWord} />
        </div>
      </div>
      <button className={styles.finishButton}>Submit</button>
    </Modal>
  );
}
