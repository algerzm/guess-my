import styles from "../../styles/InputFile.module.css";

export default function InputFile({ idName }) {
  return (
    <>
      <input type="file" id={idName} className={styles.hidden} />
      <label
        style={{
          backgroundImage: "url(/avatar.png)",
        }}
        className={styles.inputFileButton}
        htmlFor={idName}
      >
        <span className={styles.buttonLabel}>Change Image</span>
      </label>
    </>
  );
}
